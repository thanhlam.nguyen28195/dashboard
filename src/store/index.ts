import { applyMiddleware, combineReducers, createStore, Store } from 'redux';

import thunk from 'redux-thunk';
import {
  tableReducer,
  ITableState,
} from '../reducers/Table';

// Create an interface for the application state
export interface IAppState {
  tables: ITableState;
}

// Create the root reducer
const rootReducer = combineReducers<IAppState>({
  tables: tableReducer
});

// Create a configure store function of type `IAppState`
export default function configureStore(): Store<IAppState, any> {
  const store = createStore(rootReducer, undefined, applyMiddleware(thunk));
  return store;
}
