import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import axios from 'axios';

import { ITableState, ITable } from '../reducers/Table';

export enum TableActionTypes {
  GET_ALL = 'GET_ALL',
}

export interface ITableGetAllAction {
  type: TableActionTypes.GET_ALL;
  tables: ITable[];
}

export type TableActions = ITableGetAllAction;

export const getAllTable: ActionCreator<
  ThunkAction<Promise<any>, ITableState, null, ITableGetAllAction>
> = () => {
  return async (dispatch: Dispatch) => {
    try {
      const response = await axios.get('http://localhost:3004/tables');
      console.log('response',response);
      dispatch({
        tables: response.data,
        type: TableActionTypes.GET_ALL,
      });
    } catch (err) {
      console.error(err);
    }
  };
};