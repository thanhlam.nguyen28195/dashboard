export interface SiderbarState {
  dashboards: [];
  templates: [];
}

export const FETCH_DATA = "FETCH_DATA";

interface FetchDataAction {
  type: typeof FETCH_DATA;
  payload: SiderbarState;
}

export type SiderbarActionType = FetchDataAction;
