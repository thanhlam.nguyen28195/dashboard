import React, { useEffect, useState } from "react";
import "./App.css";
import { Layout } from "antd";
import HeaderDashboard from "./components/Header";
import SiderBar from "./components/SiderBar";
import {callApi} from "./utils/callApi";
import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import { HashRouter as Router } from "react-router-dom";
import { API_URL } from "./constants/configLocal";
import { DndProvider } from 'react-dnd'
import Backend from 'react-dnd-html5-backend'

const App: React.SFC = () => {
  const [tdata, setTdata] = useState({});
  useEffect(() => {
    callApi(API_URL, "siderbar").then((rp: any) => {
      return setTdata(rp.data);
    });
  }, []);

  if (Object.keys(tdata).length) {
    return (
      <Router>
        <div className="App">
          <Layout>
            <HeaderDashboard />
            <Layout>
              <SiderBar tdata={tdata} />
              <DndProvider backend={Backend}>
                <Home />
              </DndProvider>
            </Layout>
          </Layout>
        </div>
      </Router>
    );
  } else {
    return <NotFound />;
  }
};
export default App;
