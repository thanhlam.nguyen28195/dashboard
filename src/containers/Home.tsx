import { Breadcrumb, Button, Divider, Layout, Row } from "antd";
import update from 'immutability-helper';
import React, { useState } from "react";
import { useDrop, XYCoord } from 'react-dnd';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getAllTable } from "../Actions/TableActions";
import Box from '../components/Box';
import TableDynamic from "../components/TableDynamic";
import Test from '../components/Test';
import { ITable } from "../reducers/Table";
import { IAppState } from "../store/index";

interface IProps {
  tables: ITable[];
  getData: any;
}

export interface DragItem {
  type: string
  id: string
  top: number
  left: number
}

export interface IState {
  boxes: { [key: string]: { top: number; left: number; content: any } }
}
const columns = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id"
  },
  {
    title: "Employee Name",
    dataIndex: "employee_name",
    key: "employee_name"
  },
  {
    title: "Employee Salary",
    dataIndex: "employee_salary",
    key: "employee_salary"
  },
  {
    title: "Employee Age",
    dataIndex: "employee_age",
    key: "employee_age"
  },
  {
    title: "Profile Image",
    dataIndex: "profile_image",
    key: "profile_image"
  }
];

const styles: React.CSSProperties = {
  width: window.innerWidth,
  height: window.innerHeight,
  border: '1px solid black',
  position: 'relative',
}

const formDrawer = [
  {
    label: "Employee Name",
    key: "employee_name",
    type: "input"
  },
  {
    label: "Employee Salary",
    key: "employee_salary",
    type: "input"
  },
  {
    label: "Employee Age",
    key: "employee_age",
    type: "input"
  },
  {
    label: "Profile Image",
    key: "profile_image",
    type: "input"
  }
];
const Home: React.FC<IProps> = props => {
  const { tables, getData } = props;
  const searchEmp = () => {
    getData();
  };
  const [boxes, setBoxes] = useState<{
    [key: string]: {
      top: number
      left: number
      content: any
    }
  }>({
    a: { top: 100, left: 80, content:<TableDynamic
      dataList={tables}
      columns={columns}
      formDrawer={formDrawer}
      searchEmp={searchEmp}
    /> },
    b: { top: 200, left: 20, content: <Test/> },
  });
  const [, drop] = useDrop({
    accept: 'box',
    drop(item: DragItem, monitor) {
      const delta = monitor.getDifferenceFromInitialOffset() as XYCoord
      const left = Math.round(item.left + delta.x)
      const top = Math.round(item.top + delta.y)
      moveBox(item.id, left, top)
      return undefined
    },
  })

  const moveBox = (id: string, left: number, top: number) => {
    setBoxes(
      update(boxes, {
        [id]: {
          $merge: { left, top },
        },
      }),
    )
  }
 
  return (
    <Layout style={{ padding: "0 24px 24px" }}>
      <Row>
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
      </Row>
      <Row>
        <Button
          type="primary"
          icon="search"
          style={{ width: "8%" }}
          onClick={searchEmp}
        >
          Search
        </Button>
      </Row>
      <Divider />
        {/* <TableDynamic
          dataList={tables}
          columns={columns}
          formDrawer={formDrawer}
          searchEmp={searchEmp}
        />
      <Divider />
      <Test/> */}

<div ref={drop} style={styles} >
      {Object.keys(boxes).map(key => {
        const { left, top, content } = boxes[key]
        return (
          <Box
            key={key}
            id={key}
            left={left}
            top={top}
            hideSourceOnDrag={true}
          >
            {content}
            <Divider />
          </Box>
        )
      })}
      </div>
    </Layout>
  );
};
const mapStateToProps = (store: IAppState) => {
  return {
    tables: store.tables.tables
  };
};
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getData: getAllTable
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(Home);
