import { combineReducers } from "redux";
import {siderbarReducer} from './siderbar'
const appReducers = combineReducers({
  siderbarReducer
});

export default appReducers;
export type AppState = ReturnType<typeof appReducers>;
