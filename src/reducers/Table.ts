import { Reducer } from "redux";
import { TableActions, TableActionTypes } from "../Actions/TableActions";

export interface ITable {
  id: string;
  employee_name: string;
  employee_salary: string;
  employee_age: string;
  profile_image: string;
}

export interface ITableState {
  readonly tables: ITable[];
}

const initialCharacterState: ITableState = {
  tables: []
};

export const tableReducer: Reducer<ITableState, TableActions> = (
  state = initialCharacterState,
  action
) => {
  switch (action.type) {
    case TableActionTypes.GET_ALL: {
      return {
        ...state,
        tables: action.tables
      };
    }
    default:
      return state;
  }
};
