import {
  FETCH_DATA,
  SiderbarState,
  SiderbarActionType
} from "../constants/siderbar";

const initialState: SiderbarState = {
  dashboards: [],
  templates: []
};
export function siderbarReducer(
  state = initialState,
  action: SiderbarActionType
): SiderbarState {
  switch (action.type) {
    case FETCH_DATA: {
      return {
        ...state,
        ...action.payload
      };
    }
    default:   
      return state;
  }
}
