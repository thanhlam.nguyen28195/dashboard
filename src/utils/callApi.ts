import axios from "axios";
import { API_URL } from "../constants/configLocal";

export function callApi(API: string, endpoint?: string) {
  if (endpoint) return axios.get(API + endpoint);
  return axios.get(API);
}
export const updatetData = (id: any, body: any) => {
  return axios({
    method:"put",
    url: `http://localhost:3004/tables/${id}`,
    data: body
  }).catch(err => {
    console.log(err);
  });
};
export const insertData = ( body: any) => {
  return axios({
    method:"post",
    url: `http://localhost:3004/tables`,
    data: body
  }).catch(err => {
    console.log(err);
  });
};
export const deleteData = ( id: string) => {
  return axios({
    method:"delete",
    url: `http://localhost:3004/tables/${id}`
  }).catch(err => {
    console.log(err);
  });
};
