import React, { useState } from "react";
import { Button, Drawer, Form, Col, Input, Row } from "antd";
import { FormComponentProps } from "antd/lib/form/Form";
import "../styles/FormDrawer.scss";
import { updatetData, deleteData, insertData } from "../utils/callApi";
interface IProps {
  form?: any;
  drawerVisible?: boolean;
  formDrawer: any[];
  onCloseDrawer: any;
  selectedItem: any;
  setSelectedItem: any;
  searchEmp: Function;
}

const FormDrawer: React.FC<IProps> = props => {
  const { getFieldDecorator } = props.form;
  const renderTypeComponent = (item: any) => {
    switch (item.key) {
      case "input":
        return <Input id={item.key} />;
      default:
        return <Input />;
    }
  };

  const setInitialValue = (item: any) => {
    const selectedItem = props.selectedItem;

    switch (item.type) {
      case "input": {
        let defaultValue;
        if (selectedItem) {
          defaultValue = selectedItem[item.key] || "";
          if (item.key === "employee_name") {
            defaultValue = selectedItem.employee_name;
          }
          if (item.key === "employee_salary") {
            defaultValue = selectedItem.employee_salary;
          }
          if (item.key === "employee_age") {
            defaultValue = selectedItem.employee_age;
          }
          if (item.key === "profile_image") {
            defaultValue = selectedItem.profile_image;
          }
          return defaultValue;
        }
      }
    }
  };
  const onCloseDrawer = () => {
    props.setSelectedItem(undefined);
    props.onCloseDrawer();
    props.form.resetFields();
  };
   const handleSubmit = (e: any, action: string) => {
    e.preventDefault();
    props.form.validateFields(async (err: any, values: any) => {
      if (!err) {
        const body = {
          employee_name: values.employee_name,
          employee_salary: values.employee_salary,
          employee_age: values.employee_age
        };

        if (action === "update") {

          await updatetData(props.selectedItem.id, body);
        } else if (action === "delete") {
          await deleteData(props.selectedItem.id);
        } else {
          await insertData(body);
        }
        props.searchEmp();
        onCloseDrawer();
      }
    });
  };
  return (
    <Drawer visible={props.drawerVisible} width={600}>
      <Form layout="vertical">
        <Row gutter={16}>
          {props.formDrawer.map((item: any) => {
            return (
              <Col key={item.key} span={12}>
                <Form.Item label={item.label}>
                  {getFieldDecorator(item.key, {
                    initialValue: setInitialValue(item)
                  })(renderTypeComponent(item))}
                </Form.Item>
              </Col>
            );
          })}
        </Row>
        <div className="drawer-buttons-layout">
          <Button onClick={onCloseDrawer}>Cancel</Button>
          &nbsp;
          {props.selectedItem ? (
            <div>
              <Button
                type="primary"
                htmlType="submit"
                onClick={e => {
                  handleSubmit(e, "update");
                }}
              >
                Update
              </Button>
              &nbsp;
              <Button
                type="primary"
                onClick={e => {
                  handleSubmit(e, "delete");
                }}
              >
                Delete
              </Button>
            </div>
          ) : (
            <Button
              type="primary"
              onClick={e => {
                handleSubmit(e, "create");
              }}
            >
              Submit
            </Button>
          )}
        </div>
      </Form>
    </Drawer>
  );
};
const WrappedFormDrawer = Form.create<IProps & FormComponentProps>()(
  FormDrawer
);
export default WrappedFormDrawer;
