import React from "react";
import { Layout, Menu, Icon } from "antd";
import { Link, Route } from "react-router-dom";
const { SubMenu } = Menu;
const { Sider } = Layout;

interface IProps {
  tdata?: any;
}
const SiderBar: React.FC<IProps> = props => {
  let a: any = [];
  Object.entries(props.tdata).forEach(([key, values]) => {
    a.push({ key, values });
  });

  return (
    <Sider width={200} style={{ background: "#001529" }}>
      <Menu
        mode="inline"
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        style={{
          height: "calc(100vh - 64px)",
          borderRight: 0,
          backgroundColor: "#001529"
        }}
      >
        {a.map((b: any) => {
          return (
            <SubMenu
              key={b.key}
              title={
                <span>
                  <Icon type="user" />
                  {b.key}
                </span>
              }
            >
              {b.values.map((value: any) => {
                return (
                  <Menu.Item key={value.siderbarId}>
                    <Route
                      path={value.link}
                      exact={value.exact}
                      children={({ match }) => {
                        return (
                          <Link to={value.link}>{value.siderbarName}</Link>
                        );
                      }}
                    />
                  </Menu.Item>
                );
              })}
            </SubMenu>
          );
        })}
      </Menu>
    </Sider>
  );
};

export default SiderBar;
