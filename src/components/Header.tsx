import React from "react";
import { Layout, Menu } from "antd";

const { Header } = Layout;

const HeaderDashboard: React.FC = () => {
  return (
    <Header className="header">
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={["2"]}
        style={{ lineHeight: "64px" }}
      >
        <Menu.Item key="1">HOME</Menu.Item>
      </Menu>
    </Header>
  );
};

export default HeaderDashboard;
