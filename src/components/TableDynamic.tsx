import React, { useState } from "react";
import { Table, Button, Drawer } from "antd";
import { ITable } from "../reducers/Table";
import FormDrawer from "./FormDrawer";

interface IProps {
  dataList: ITable[];
  columns: any[];
  formDrawer: any[];
  searchEmp:Function;
}

const TableDynamic: React.FC<IProps> = props => {
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [selectedItem, setSelectedItem] = useState(undefined);
  const { dataList, columns } = props;
  const onRow = (record: any) => {
    return {
      onClick: () => {
        setDrawerVisible(true);
        setSelectedItem(record);
      }
    };
  };
  const onCloseDrawer = () => {
    setDrawerVisible(false);
  };
  const addEmp = () => {
    setDrawerVisible(true);
  };
  return (
    <div>
      <Button
        type="dashed"
        icon="plus"
        style={{ width: "8%" }}
        onClick={addEmp}
      >
        Add
      </Button>
      <br />
      <Table
        dataSource={dataList}
        columns={columns}
        size="small"
        onRow={onRow}
      />
      <FormDrawer
        drawerVisible={drawerVisible}
        formDrawer={props.formDrawer}
        onCloseDrawer={onCloseDrawer}
        selectedItem={selectedItem}
        setSelectedItem={setSelectedItem}
        searchEmp={props.searchEmp}
      />
    </div>
  );
};

export default TableDynamic;
